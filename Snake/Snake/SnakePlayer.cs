﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake
{
    /// <summary>
    /// Обьявление Игрока
    /// </summary>
    public enum Direction
    {
        left,
        right,
        up,
        down,
        none
    }

    /// <summary>
    /// Класс, содержащий логику контроллера для игрока
    /// </summary>
    class SnakePlayer
    {
        private List<BodyPart> m_SnakeParts = new List<BodyPart>(); // Коллекция текущих частей тела змеи
        private const int m_CircleRadius = 20; // Определяет размер части тела
        private Direction m_MoveDirection = Direction.none; // Направление головы
        private int m_PendingSegments; // Количество частей тела в очереди, которые будут добавлены к змее
        private SnakeForm GameForm = null; // Хранит форму графического интерфейса

        public SnakePlayer(SnakeForm Form)
        {
            // Добавляет змее в начале 3 элемента тела
            m_SnakeParts.Add(new BodyPart(100, 0, Direction.right));
            m_SnakeParts.Add(new BodyPart(80, 0, Direction.right));
            m_SnakeParts.Add(new BodyPart(60, 0, Direction.right));

            // Даёт начальное направление
            m_MoveDirection = Direction.right;

            // Количество частей тел в очереди - 0
            m_PendingSegments = 0;
            GameForm = Form;
        }

        /// <summary>
        /// Добавление элементов тела
        /// </summary>
        /// <param name="Number">Номер элемента в очереди</param>
        public void AddBodySegments(int Number)
        {
            // Увеличивает m_PendingSegments, так как он будет обработан, а части добавлены и будут обработаны в MovePlayer()
            m_PendingSegments += Number;
        }

        /// <summary>
        /// Перемещает змею и обрабатывает любые ожидающие сегменты тела, которые будут созданы. Называет каждый кадр.
        /// </summary>
        public void MovePlayer()
        {
            // Обработка частей тела. Только 1й за раз!
            // если m_PendingSegments > 1, для полной обработки потребуется более одного кадра.
            if (m_PendingSegments > 0)
            {
                Point LastPos = m_SnakeParts.Last().GetPosition(); // Добавление 1 элемента
                m_SnakeParts.Add(new BodyPart(LastPos.X,LastPos.Y));
                m_PendingSegments--;
            }
            
            m_SnakeParts[0].m_Dir = m_MoveDirection; // Установка позиции головы

            // Пермещает каждую часть тела змеи
            for (int i = m_SnakeParts.Count-1; i>=0 ;i--)
            {
                // Переводит часть тела в соответствии с его направлением
                switch (m_SnakeParts[i].m_Dir)
                {
                    case Direction.left:
                        m_SnakeParts[i].AddPosition(new Point(-20, 0));
                        break;
                    case Direction.right:
                        m_SnakeParts[i].AddPosition(new Point(20, 0));
                        break;
                    case Direction.down:
                        m_SnakeParts[i].AddPosition(new Point(0, 20));
                        break;
                    case Direction.up:
                        m_SnakeParts[i].AddPosition(new Point(0, -20));
                        break;
                    default:
                        break;
                }
                if (i > 0)
                        m_SnakeParts[i].m_Dir = m_SnakeParts[i - 1].m_Dir;
            }
            if (IsSelfIntersecting()) // Проверка на столкновение с самим собой
                OnHitSelf(); // если это оно имеет значение true - вывод окна проигрыша
        }

        /// <summary>
        /// Проверка пересичения с самой собой
        /// </summary>
        public bool IsSelfIntersecting()
        {
            // Проверка каждой части тела с другой частью тела
            for(int i=0;i < m_SnakeParts.Count;i++)
            {
                for (int j = 0;j < m_SnakeParts.Count; j++)
                {
                    if(i == j)
                        continue;
                    BodyPart part1 = m_SnakeParts[i];
                    BodyPart part2 = m_SnakeParts[j];

                    // Логика проверки на столкновение
                    if ((new Rectangle(part1.GetPosition().X, part1.GetPosition().Y, m_CircleRadius, m_CircleRadius)).IntersectsWith(
                        new Rectangle(part2.GetPosition().X, part2.GetPosition().Y, m_CircleRadius, m_CircleRadius)))
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Установка направления головы
        /// </summary>
        /// <param name="Dir">Направление для установки</param>
        public void SetDirection(Direction Dir)
        {
            // Запрет поворота на 180
            if (m_MoveDirection == Direction.left && Dir == Direction.right)
                return;

            if (m_MoveDirection == Direction.right && Dir == Direction.left)
                return;

            if (m_MoveDirection == Direction.up && Dir == Direction.down)
                return;

            if (m_MoveDirection == Direction.down && Dir == Direction.up)
                return;

            // Установка направления, если его изменение допустимо
            m_MoveDirection = Dir;
        }

        /// <summary>
        /// Определяет, пересекается ли какая-либо часть тела с данным прямоугольником
        /// </summary>
        /// <param name="rect">Прямоугольник для проверки пересечений</param>
        /// <returns>Было ли пересечение</returns>
        public bool IsIntersectingRect(Rectangle rect)
        {
            foreach(BodyPart Part in m_SnakeParts) // Проверка каждой части тела змеи
            {
                Point PartPos = Part.GetPosition();

                // Проверка пересечения прямоугольника с частью тела
                if (rect.IntersectsWith(new Rectangle(PartPos.X, PartPos.Y, m_CircleRadius, m_CircleRadius)))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Вывод "окончания игры" после удара о стену
        /// </summary>
        /// <param name="WhichWall">Направление удареной стены</param>
        public void OnHitWall(Direction WhichWall)
        {
            GameForm.ToggleTimer(); // скрытие таймера в данном окне
            MessageBox.Show("Вы столкнулись со стеной - GAME OVER");
            GameForm.ResetGame();
        }

        /// <summary>
        /// Вызывается, для визуализации змеи
        /// </summary>
        /// <param name="canvas">Граф. обьект для рендеринга</param>
        public void Draw(Graphics canvas)
        {
            Brush SnakeColor = Brushes.Black;
            List<Rectangle> Rects = GetRects(); // Получаем части тела змеи, представленные в виде прямоугольников
            foreach (Rectangle Part in Rects) // Рисуем каждую часть змеи
            {
                canvas.FillEllipse(SnakeColor, Part); // Рисуем части ввиде элипсов
            }
        }

        /// <summary>
        /// вывод "окончания игры" после удара об себя же
        /// </summary>
        public void OnHitSelf()
        {
            GameForm.ToggleTimer(); // скрытие таймера в данном окне
            MessageBox.Show("Вы ударились о себя - GAME OVER"); 
            GameForm.ResetGame();
        }

        /// <summary>
        /// Получаем части тела змеи, представленные в виде прямоугольников
        /// </summary>
        /// <returns>Список частей тела змеи, представленных в виде прямоугольников</returns>
        public List<Rectangle> GetRects()
        {
            List<Rectangle> Rects = new List<Rectangle>();
            foreach (BodyPart Part in m_SnakeParts) // Возврат всех частей тела
            {
                Point PartPos = Part.GetPosition();

                // На каждой итерации добавляем прямоугольник в текущий список, представляющий часть тела
                Rects.Add(new Rectangle(PartPos.X, PartPos.Y, m_CircleRadius, m_CircleRadius));
            }
            return Rects;
        } 
    }
}

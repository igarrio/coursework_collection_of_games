﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Snake
{
    /// <summary>
    /// Manages food pellets, including spawning, destruction, and collision detection
    /// </summary>
    class FoodManager
    {
        private Random r = new Random(); //Используется для генерации случайных величин в этом классе
        private List<FoodPellet> m_FoodPellets = new List<FoodPellet>(); //Сбор всей "еды" в игре
        private const int m_CircleRadius = 20; //Установка размера "еды"
        private int m_GameWidth; //Размер экрана в пикселях, дабы игра могла рисовать элементы
        private int m_GameHeight;

        /// <summary>
        /// Конструктор обьектов
        /// </summary>
        /// <param name="GameWidth">Pixel width of the game window</param>
        /// <param name="GameHeight">Pixel height of the game window</param>
        public FoodManager(int GameWidth,int GameHeight)
        {
            m_GameWidth = GameWidth;
            m_GameHeight = GameHeight;
        }

        /// <summary>
        /// Рисовка "еды"
        /// </summary>
        /// <param name="Canvas">Canvas object (game screen) to draw on</param>
        public void Draw(Graphics Canvas)
        {
            //Сортировка всей "еды" для дальнейшего их рисования
            Brush SnakeColor = Brushes.Red;
            foreach (FoodPellet Pellet in m_FoodPellets)
            {
                Point PartPos = Pellet.GetPosition();
                Canvas.FillEllipse(SnakeColor, new Rectangle(PartPos.X+(m_CircleRadius / 4), PartPos.Y+ (m_CircleRadius / 4), m_CircleRadius/2, m_CircleRadius/2));
            }
        }

        /// <summary>
        /// Adds a food pellet to the game
        /// </summary>
        public void AddRandomFood()
        {
            int X = r.Next(m_GameWidth - m_CircleRadius); // Установка рандомной позиции х/у
            int Y = r.Next(m_GameHeight - m_CircleRadius);
            int ix = (X / m_CircleRadius); //Использование усечения для привязки к сетке
            int iy = Y / m_CircleRadius;
            X = ix * m_CircleRadius; // Сетка позиций для х/у
            Y = iy * m_CircleRadius;
            m_FoodPellets.Add(new FoodPellet(X, Y)); //Сохраняем "еду"
        }

        /// <summary>
        /// Добавление "еды"
        /// </summary>
        /// <param name="Amount">Amount of food to add</param>
        public void AddRandomFood(int Amount)
        {
            for(int i=0;i<Amount;i++)
            {
                AddRandomFood();
            }
        }

        /// <summary>
        /// Проверка пересечения двух гранул "еды"
        /// </summary>
        /// <param name="rect">The rectangle to check for collision with food pellets</param>
        /// <param name="RemoveFood">Whether to remove the food pellets intersecting with the rectangle</param>
        /// <returns>Whether there was an intersection</returns>
        public bool IsIntersectingRect(Rectangle rect, bool RemoveFood)
        {
            foreach (FoodPellet Pellet in m_FoodPellets) // Проверка каждой гранулы
            {
                Point PartPos = Pellet.GetPosition();

                // Проверка, пересекается ли "прямоугольник" с "едой"
                if (rect.IntersectsWith(new Rectangle(PartPos.X, PartPos.Y, m_CircleRadius, m_CircleRadius)))
                {
                    if (RemoveFood) // Удаление гранулы если RemoveFood имеет значение true
                        m_FoodPellets.Remove(Pellet);
                    return true;
                }
            }
            return false;
        }
    }
}

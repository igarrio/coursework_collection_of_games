﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake
{
    /// <summary>
    /// Ввод с клавиатуры в игру
    /// </summary>
    class Input
    {
        // Сохраняет отображение клавиш клавиатуры на то, нажата ли кнопка
        private static Dictionary<Keys,bool> KeyTable = new Dictionary<Keys,bool>();

        /// <summary>
        /// Проверка на Удержание клавиши
        /// </summary>
        /// <param name="key">Кнопка, над которой проводят проверку</param>
        public static bool IsKeyDown(Keys key)
        {
            bool KeyState;
            if (KeyTable.TryGetValue(key,out KeyState))
            {
                return KeyState;
            }
            return false;
        }

        /// <summary>
        /// Проверка клавиши на нажатие
        /// </summary>
        public static void SetKey(Keys key,bool IsDown)
        {
            KeyTable[key] = IsDown;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    class GamePart
    {
        private Point Position;

        /// <summary>
        /// Получаем координаты
        /// </summary>
        /// <returns>Position of the point</returns>
        public Point GetPosition()
        {
            return Position;
        }

        /// <summary>
        /// Перемещение
        /// </summary>
        /// <param name="point">Point to add</param>
        public void AddPosition(Point point)
        {
   
            Position.X += point.X;
            Position.Y += point.Y;
        }

        /// <summary>
        /// Установка позиции
        /// </summary>
        /// <param name="point">Point to set</param>
        public void SetPosition(Point point)
        {
    
            Position = point;
        }

        /// <summary>
        /// Конструктор обьекта
        /// </summary>
        /// <param name="X">X coordinate of the part</param>
        /// <param name="Y">Y coordinate of the part</param>
        public GamePart(int X,int Y)
        {
            Position = new Point(X,Y);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using MetroFramework.Forms;
namespace casino_support
{
    public partial class Form_start : MetroForm
    {
        private static Telegram.Bot.TelegramBotClient BOT;

        public Form_start()
        {
            InitializeComponent();
        }

        private void Button_start_bot_Click(object sender, EventArgs e)
        {
            BOT = new Telegram.Bot.TelegramBotClient("835915422:AAEOMDh79JP_VmfhEOSVN42zY4aTEZXloNU");
            BOT.OnMessage += BotOnMessageReceived;
            BOT.StartReceiving(new UpdateType[] { UpdateType.Message });
            var me = BOT.GetMeAsync().Result;
            button_start_bot.Enabled = false;
        }

        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            Telegram.Bot.Types.Message msg = messageEventArgs.Message;
            if (msg == null || msg.Type != MessageType.Text) return;

            String Answer = "";

            switch (msg.Text)
            {
                case "/start": Answer = "Это - бот поддержки приложения NUZPGames 🔥\n Любые вопросы можно решить здесь!\nСписок комманд:\n/how_to_use - Как пользоваться приложением\n/payments_help - Помощь при проблемах с платежами\n/system_help - Помощь при системных проблемах"; break;
                case "/how_to_use": Answer = "Для начала, вам нужно зарегистрироваться/войти. Если с этим появились проблемы - /system_help\nДальше у вас откроется Главное меню. В нём вы можете увидеть 2 (пока что) игры на ваш вкус.\nИнтерфейс интуитивно понятен и прост. Приятной вам игры ☺️"; break;
                case "/system_help": Answer = "Для решения системных проблем писать следующим лицам:\n1️⃣ Проблемы с регистрацией, главным меню, не работают кнопки и тд. - @igarrio\n2️⃣ Любые проблемы связанные с играми и выводом оттуда денег и тд. - @@vitalijbrezhnev"; break;
                case "/payments_help": Answer = "Из-за того, что с этим проблем не возникало - чёткого списка возможных нет.\n\nЕсли они всё же возникли, пишите главному администратору приложения - @igarrio"; break;
                case "💵": Answer = "А это то, что не потребуется для наших игр 😉"; break;
                case "😘": Answer = "Мы тоже рады тебя видеть! ❤️"; break;
                case "🌚": Answer = "🌝 Самое время поиграть!"; break;
                default: Answer = "Увы, я не понимаю данной команды 🧐\n Проверьте, правильно ли вы её ввели и отправьте ещё раз 😉"; break;
            }
            await BOT.SendTextMessageAsync(msg.Chat.Id, Answer);
        }
    }
} 


﻿namespace casino_support
{
    partial class Form_start
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_start_bot = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_start_bot
            // 
            this.button_start_bot.Location = new System.Drawing.Point(24, 63);
            this.button_start_bot.Name = "button_start_bot";
            this.button_start_bot.Size = new System.Drawing.Size(270, 46);
            this.button_start_bot.TabIndex = 0;
            this.button_start_bot.Text = "Start BOT";
            this.button_start_bot.UseVisualStyleBackColor = true;
            this.button_start_bot.Click += new System.EventHandler(this.Button_start_bot_Click);
            // 
            // Form_start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 144);
            this.Controls.Add(this.button_start_bot);
            this.MaximumSize = new System.Drawing.Size(317, 144);
            this.MinimumSize = new System.Drawing.Size(317, 144);
            this.Name = "Form_start";
            this.Text = "Support BOT";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_start_bot;
    }
}


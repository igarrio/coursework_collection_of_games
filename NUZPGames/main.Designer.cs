﻿namespace CasinoNUZP
{
    partial class main
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.vvod = new MetroFramework.Controls.MetroButton();
            this.game3 = new MetroFramework.Controls.MetroButton();
            this.game2 = new MetroFramework.Controls.MetroButton();
            this.linkSupport = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // vvod
            // 
            this.vvod.Location = new System.Drawing.Point(317, 28);
            this.vvod.Name = "vvod";
            this.vvod.Size = new System.Drawing.Size(132, 33);
            this.vvod.TabIndex = 1;
            this.vvod.Text = "Поддержать проект";
            this.vvod.UseSelectable = true;
            this.vvod.Click += new System.EventHandler(this.Vvod_Click_1);
            // 
            // game3
            // 
            this.game3.Location = new System.Drawing.Point(23, 80);
            this.game3.Name = "game3";
            this.game3.Size = new System.Drawing.Size(210, 75);
            this.game3.TabIndex = 3;
            this.game3.Text = "BlackJack";
            this.game3.UseSelectable = true;
            this.game3.Click += new System.EventHandler(this.Game3_Click);
            // 
            // game2
            // 
            this.game2.Location = new System.Drawing.Point(242, 80);
            this.game2.Name = "game2";
            this.game2.Size = new System.Drawing.Size(210, 75);
            this.game2.TabIndex = 5;
            this.game2.Text = "Змейка. Сыграй в класcику)";
            this.game2.UseSelectable = true;
            this.game2.Click += new System.EventHandler(this.Game2_Click);
            // 
            // linkSupport
            // 
            this.linkSupport.AutoSize = true;
            this.linkSupport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkSupport.LinkColor = System.Drawing.Color.Teal;
            this.linkSupport.Location = new System.Drawing.Point(23, 173);
            this.linkSupport.Name = "linkSupport";
            this.linkSupport.Size = new System.Drawing.Size(394, 16);
            this.linkSupport.TabIndex = 7;
            this.linkSupport.TabStop = true;
            this.linkSupport.Text = "Возникли проблемы? Обращайся к нашему боту в Telegram";
            this.linkSupport.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkSupport_LinkClicked);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(475, 217);
            this.Controls.Add(this.linkSupport);
            this.Controls.Add(this.game2);
            this.Controls.Add(this.game3);
            this.Controls.Add(this.vvod);
            this.MaximumSize = new System.Drawing.Size(475, 217);
            this.MinimumSize = new System.Drawing.Size(475, 217);
            this.Movable = false;
            this.Name = "main";
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.Text = "NUZPGames";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroButton vvod;
        private MetroFramework.Controls.MetroButton game3;
        private MetroFramework.Controls.MetroButton game2;
        private System.Windows.Forms.LinkLabel linkSupport;
    }
}


﻿namespace CasinoNUZP
{
    partial class vvod_money
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.vvod_bc = new MetroFramework.Controls.MetroButton();
            this.vvod_qiwi = new MetroFramework.Controls.MetroButton();
            this.vvod_wm = new MetroFramework.Controls.MetroButton();
            this.vvod_payeer = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // vvod_bc
            // 
            this.vvod_bc.Location = new System.Drawing.Point(24, 63);
            this.vvod_bc.Name = "vvod_bc";
            this.vvod_bc.Size = new System.Drawing.Size(285, 53);
            this.vvod_bc.TabIndex = 1;
            this.vvod_bc.Text = "С банковской карты";
            this.vvod_bc.UseSelectable = true;
            this.vvod_bc.Click += new System.EventHandler(this.Vvod_bc_Click);
            // 
            // vvod_qiwi
            // 
            this.vvod_qiwi.Location = new System.Drawing.Point(23, 122);
            this.vvod_qiwi.Name = "vvod_qiwi";
            this.vvod_qiwi.Size = new System.Drawing.Size(286, 53);
            this.vvod_qiwi.TabIndex = 2;
            this.vvod_qiwi.Text = "Qiwi";
            this.vvod_qiwi.UseSelectable = true;
            this.vvod_qiwi.Click += new System.EventHandler(this.Vvod_qiwi_Click);
            // 
            // vvod_wm
            // 
            this.vvod_wm.Location = new System.Drawing.Point(23, 181);
            this.vvod_wm.Name = "vvod_wm";
            this.vvod_wm.Size = new System.Drawing.Size(286, 53);
            this.vvod_wm.TabIndex = 3;
            this.vvod_wm.Text = "WebMoney";
            this.vvod_wm.UseSelectable = true;
            this.vvod_wm.Click += new System.EventHandler(this.Vvod_wm_Click);
            // 
            // vvod_payeer
            // 
            this.vvod_payeer.Location = new System.Drawing.Point(24, 240);
            this.vvod_payeer.Name = "vvod_payeer";
            this.vvod_payeer.Size = new System.Drawing.Size(285, 53);
            this.vvod_payeer.TabIndex = 4;
            this.vvod_payeer.Text = "Payeer";
            this.vvod_payeer.UseSelectable = true;
            this.vvod_payeer.Click += new System.EventHandler(this.Vvod_payeer_Click);
            // 
            // vvod_money
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 318);
            this.Controls.Add(this.vvod_payeer);
            this.Controls.Add(this.vvod_wm);
            this.Controls.Add(this.vvod_qiwi);
            this.Controls.Add(this.vvod_bc);
            this.MaximumSize = new System.Drawing.Size(332, 318);
            this.MinimumSize = new System.Drawing.Size(332, 318);
            this.Name = "vvod_money";
            this.Text = "Пожертвовать на развитие";
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroButton vvod_bc;
        private MetroFramework.Controls.MetroButton vvod_qiwi;
        private MetroFramework.Controls.MetroButton vvod_wm;
        private MetroFramework.Controls.MetroButton vvod_payeer;
    }
}
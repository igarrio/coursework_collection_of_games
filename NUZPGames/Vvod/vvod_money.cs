﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace CasinoNUZP
{
    public partial class vvod_money : MetroForm
    {
        public vvod_money()
        {
            InitializeComponent();
        }

        private void Vvod_qiwi_Click(object sender, EventArgs e)
        {
            this.Close();
            vvod_qiwi f = new vvod_qiwi();
            f.ShowDialog();
        }

        private void Vvod_payeer_Click(object sender, EventArgs e)
        {
            this.Close();
            vvod_payeer f = new vvod_payeer();
            f.ShowDialog();
        }

        private void Vvod_bc_Click(object sender, EventArgs e)
        {
            this.Close();
            vvod_bc f = new vvod_bc();
            f.ShowDialog();
        }

        private void Vvod_wm_Click(object sender, EventArgs e)
        {
            this.Close();
            vvod_wm f = new vvod_wm();
            f.ShowDialog();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace CasinoNUZP
{
    public partial class Reg : MetroForm
    {
        public Reg()
        {
            InitializeComponent();
        }
        
        private void RegButton_Click(object sender, EventArgs e)
        {
            string connectionString = ConfigurationManager.AppSettings["connectionString"];
            SqlConnection sql = new SqlConnection(connectionString);

            try
            {
                if (sql.State == System.Data.ConnectionState.Closed)
                    sql.Open();

                string query = "SELECT COUNT(1) FROM UsersDB WHERE Login=@Login AND Password=@Password";
                SqlCommand sqlCom = new SqlCommand(query, sql);
                sqlCom.CommandType = System.Data.CommandType.Text;
                sqlCom.Parameters.Add("@Login", LoginTextBox.Text);
                sqlCom.Parameters.Add("@Password", PasswordTextBox.Text);

                int countUser = Convert.ToInt32(sqlCom.ExecuteScalar());
                if (countUser == 0)
                {
                    query = "INSERT INTO UsersDB(Login, Password) VALUES(@Login, @Password)";
                    SqlCommand Com = new SqlCommand(query, sql);
                    Com.CommandType = System.Data.CommandType.Text;
                    Com.Parameters.Add("@Login", LoginTextBox.Text);
                    Com.Parameters.Add("@Password", PasswordTextBox.Text);
                    Com.ExecuteNonQuery();
                    MessageBox.Show("Войдите в приложение для дальнейшей работы", "Новый аккаунт зарегистрирован!");
                    this.Hide();
                    sign f = new sign();
                    f.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                sql.Close();
            }
        }

        private void MetroButton1_Click(object sender, EventArgs e)
        {
            this.Hide();
            sign f = new sign();
            f.Show();
        }

        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://t.me/nuzpgames_support_bot");
        }
    }
}

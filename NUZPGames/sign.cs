﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace CasinoNUZP
{
    public partial class sign : MetroForm
    {
        public sign()
        {
            InitializeComponent();
        }

        private SqlConnection sqlConnection = null;

        protected async void Page_Load(object sender, EventArgs e)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            sqlConnection = new SqlConnection(connectionString);
            await sqlConnection.OpenAsync();
        }

        private void SignButton_Click(object sender, EventArgs e)
        {
            bool auth = false;
            using (var connection = new SqlConnection(@"Data Source=IHOR-EISMONT\MSSQLSERVER02;Initial Catalog=PersonDB;Integrated Security=True;Pooling=False"))
            {
                var command = new SqlCommand("SELECT COUNT(*) FROM UsersDB WHERE Login = @Login and Password = @Password", connection);
                command.Parameters.AddWithValue("@Login", LoginTextBox.Text);
                command.Parameters.AddWithValue("@Password", PasswordTextBox.Text);
                connection.Open();
                auth = (int)command.ExecuteScalar() == 1;
            }

            if (auth)
            {
                MessageBox.Show("Вы вошли в свой профиль");
                this.Hide();
                main f = new main();
                f.Show();
            }
            else
            {
                MessageBox.Show("Неверный пароль или логин. Попробуйте пожалуйста ещё :(");
            }
        }

        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://t.me/nuzpgames_support_bot");
        }
    }
}
